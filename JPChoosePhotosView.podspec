Pod::Spec.new do |s|

s.name         = "JPChoosePhotosView"

s.version      = "0.1.0"

s.summary      = "初步调试完毕"

s.description  = <<-DESC
   JPChoosePhotosView对TZImagePickerController进行封装，更加快速构建选取系统相册图片
DESC

s.homepage     = "https://gitee.com/singleCarpenter/JPChoosePhotosView"

s.license      = { :type => "MIT", :file => "LICENSE" }

s.author       = { "Carpenter" => "158287481@qq.com" }

s.platform     = :ios, "9.0"

s.source       = { :git => "git@gitee.com:singleCarpenter/JPChoosePhotosView.git", :tag => s.version }

s.public_header_files = "JPChoosePhotosView/*.h"

s.source_files = "JPChoosePhotosView/*.{h,m}"

s.resources   = "JPChoosePhotosView/*.bundle"

s.frameworks = "UIKit", "Foundation"

s.requires_arc = true

s.dependency 'Masonry'

s.dependency 'TZImagePickerController' 

end

