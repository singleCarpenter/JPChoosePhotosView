//
//  ViewController.m
//  JPChoosePhotosViewDemo
//
//  Created by Carpenter on 2018/2/12.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "ViewController.h"
#import "JPChoosePhotosView.h"
#import <Masonry/Masonry.h>

@interface ViewController ()

@end

@implementation ViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    
    JPPhotosConfigure * configure = [JPPhotosConfigure new];
    configure.viewWidth = self.view.frame.size.width - 20;
    
   JPChoosePhotosView * photoView = [JPChoosePhotosView jp_sheardChoosePhotosViewWithPhotosConfigure:configure];
 
    photoView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
 
    photoView.choosePhotosViewDidFinishPickingPhotosHandle = ^(NSArray<UIImage *> *photos) {
 
        NSLog(@"choosedPhotos = %@",photos);
    };
    
    [self.view addSubview:photoView];
    
    [photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(200);
        make.centerX.mas_equalTo(0);
    }];
    
}


@end
