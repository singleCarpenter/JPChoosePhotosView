//
//  JPPhotosConfigure.m
//  JPChoosePhotosViewDemo
//
//  Created by Carpenter on 2018/2/13.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPPhotosConfigure.h"

@implementation JPPhotosConfigure


-(CGSize)itemSize{
    
    CGFloat width = (self.viewWidth - (self.horizontalClearance * ( self.rowNumber + 1))) / self.rowNumber;

    CGFloat height = width * self.aspectRatio;
    
    return CGSizeMake(width, height);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.viewWidth = [UIScreen mainScreen].bounds.size.width;
        
        self.rowNumber = 3;
        
        self.maxNum = 9;
     
        self.verticalClearance = 10.0;
        
        self.horizontalClearance = 10.0;
 
        self.aspectRatio = 1;
        
        self.imageCornerRadius = 5.0;
        
        self.addphotoImage = [self getJPPhotoBundleImage:@"placeholder"];
        
        self.deleteImage = [self getJPPhotoBundleImage:@"delete"];
        
        self.videoImage = [self getJPPhotoBundleImage:@"video"];
 
    }
    return self;
}
-(void)setMaxNum:(int)maxNum{
    if (self.isVideo) {
        maxNum = 1;
    }
    _maxNum = maxNum;
}
//获取JPSuspension.bundle下资源图片
-(UIImage*)getJPPhotoBundleImage:(NSString*)name{
    
    NSBundle * bundle = [NSBundle bundleForClass:[JPPhotosConfigure class]];
    
    NSURL * url = [bundle URLForResource:@"JPPhoto" withExtension:@"bundle"];
    
    if (url) {
        bundle = [NSBundle bundleWithURL:url];
    };
    
    int scale = (int)[UIScreen mainScreen].scale;
    
    NSString * imageName = [NSString stringWithFormat:@"%@@%dx",name,scale];
    
    NSString * path = [bundle pathForResource:imageName ofType:@"png"];
    
    UIImage * image = [UIImage imageWithContentsOfFile:path];
    
    return image;
    
}
@end
