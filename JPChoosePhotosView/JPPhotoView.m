//
//  JPPhotoView.m
//  JPChoosePhotosViewDemo
//
//  Created by Carpenter on 2018/2/12.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPPhotoView.h"
#import <Masonry/Masonry.h>

@implementation JPPhotoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
 
        self.photoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.photoBtn setBackgroundColor:[UIColor whiteColor]];
//        self.photoBtn.layer.cornerRadius = 5;
        self.photoBtn.clipsToBounds = true;
        self.photoBtn.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:self.photoBtn];
        [self.photoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(0);
        }];
        
        self.videoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:self.videoButton];
        [self.videoButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(0);
            make.size.mas_equalTo(40);
        }];
        
        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.deleteBtn.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:self.deleteBtn];
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.photoBtn.mas_top).offset(-10);
            make.right.mas_equalTo(self.photoBtn.mas_right).offset(10);
            make.width.height.mas_greaterThanOrEqualTo(20);
        }];
        
        
        self.videoButton.hidden = true;
    }
    return self;
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *view = [super hitTest:point withEvent:event];
    if (view == nil) {
        // 转换坐标系
        CGPoint newPoint = [self.deleteBtn convertPoint:point fromView:self];
        // 判断触摸点是否在button上
        if (CGRectContainsPoint(self.deleteBtn.bounds, newPoint)) {
            view = self.deleteBtn;
        }
    }
    return view;
 
}

@end
