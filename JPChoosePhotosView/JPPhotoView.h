//
//  JPPhotoView.h
//  JPChoosePhotosViewDemo
//
//  Created by Carpenter on 2018/2/12.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPPhotoView : UIView

/**
 选取的图片按钮
 */
@property (nonatomic ,strong) UIButton * photoBtn;
    
/**
 删除按钮
 */
@property (nonatomic, strong) UIButton *deleteBtn;

/**
 视频按钮
 */
@property (nonatomic ,strong) UIButton * videoButton;

@end
