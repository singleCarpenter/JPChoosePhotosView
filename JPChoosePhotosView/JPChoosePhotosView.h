//
//  JPChoosePhotosView.h
//  JPChoosePhotosViewDemo
//
//  Created by Carpenter on 2018/2/12.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TZImagePickerController/TZImagePickerController.h>
#import <Masonry/Masonry.h>
#import "JPPhotoView.h"
#import "JPPhotosConfigure.h"

typedef BOOL(^JPPhotoViewWillOpenImagePickerControllerBlock)(void);


@interface JPChoosePhotosView : UIView



/**
 创建定制视图

 @param configure 定制信息
 @return 视图
 */
+(instancetype)jp_sheardChoosePhotosViewWithPhotosConfigure:(JPPhotosConfigure*)configure;

/**
 配置信息
 */
@property (nonatomic ,strong ,readonly) JPPhotosConfigure * configure;
 
/**
 即将打开图片选择器回调
 */
@property (nonatomic ,copy) JPPhotoViewWillOpenImagePickerControllerBlock  jp_willPresentImagePickerControllerBlock;


/**
 选取图片回调 
 */
@property (nonatomic, copy) void (^choosePhotosViewDidFinishPickingPhotosHandle)(NSArray<UIImage *> *photos);

/**
 选取视频回调
 */
@property (nonatomic, copy) void (^choosePhotosViewDidFinishPickingVideosHandle)(NSArray<UIImage *> *photos ,NSArray<PHAsset *> * assets);

/**
 预估尺寸
 */
-(CGSize)jp_estimatedSize;

/**
 刷新界面
 */
-(void)reloadData;

@end
