//
//  JPPhotosConfigure.h
//  JPChoosePhotosViewDemo
//
//  Created by Carpenter on 2018/2/13.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger ,JPPhotoDeleteType) {
    /*!  */
    JPPhotoDeleteTypeDefalts,
    /*! 右上角 */
    JPPhotoDeleteTypeRightTop,
};


@interface JPPhotosConfigure : NSObject

/**
 图片圆角，默认5pt
 */
@property (nonatomic ,assign) CGFloat imageCornerRadius;

/**
 视图宽度（默认为屏幕宽度）
 */
@property (nonatomic ,assign) CGFloat viewWidth;

/**
 单元格宽高比，默认1：1
 */
@property (nonatomic ,assign) CGFloat aspectRatio;

/**
 每行显示图片数量（默认3个）
 */
@property (nonatomic ,assign) int rowNumber;

/**
 最大显示图片数量（默认9个）
 */
@property (nonatomic ,assign) int maxNum;

/**
 是否选取视频，只支持选取一个
 */
@property (nonatomic ,assign) BOOL isVideo;

/**
 视图间水平间距(默认10个点)
 */
@property (nonatomic ,assign) CGFloat verticalClearance;

/**
 视图间垂直间距(默认10个点)
 */
@property (nonatomic ,assign) CGFloat horizontalClearance;

/**
 删除按钮位置
 */
@property (nonatomic ,assign) JPPhotoDeleteType deleteType;

/**
 添加图片图标
 */
@property (nonatomic,strong)UIImage * addphotoImage;

/**
 删除图片图标
 */
@property (nonatomic,strong)UIImage * deleteImage;

/**
 视频图标
 */
@property (nonatomic,strong)UIImage * videoImage;

/**
 通过计算得出需要显示的图片视图大小
 */
@property (nonatomic ,assign ,readonly) CGSize itemSize;



@end
